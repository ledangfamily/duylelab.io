/**
 * This script hides the title 'Le Dang' by sliding up slowly
 * @version: Sep 16, 2017
 */

 $(document).ready(()=>{
    $("#ledangvn").css("font-size", "3em");
    $("#ledangvn").show("slide", {direction: "left"}, 1500);
    // $("#ledangvn").slideUp(2500);
 });