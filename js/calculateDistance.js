/**
 * Method to calculate the shortest distance over the 
 * earth's surface of two locations based on their 
 * latitude and longitude
 * @version: Oct 13, 2017
 */

'use strict'
const R = 6371; //km

/**
 * This is the latitude and longitude of the Le Dang family house 
 * in Quảng Lập, Đơn Phương, Lâm Đồng province Viet Nam
 */
const lat2 = 11.7576719;
const lon2 = 108.5034441

// User's location
// let lat1 = 38.921171196;
let lat1 = 0;
// let lon1 = -94.5573946;
let lon1 = 6;
let distance = 0;

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    lat1 = position.coords.latitude;
    lon1 = position.coords.longitude;
    calcDistance();
}

Number.prototype.toRad = function() {
    return this * Math.PI / 180;
}

function calcDistance() {
    var x1 = lat2-lat1;
    var dLat = x1.toRad();  
    var x2 = lon2-lon1;
    var dLon = x2.toRad();  
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
                    Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
                    Math.sin(dLon/2) * Math.sin(dLon/2);  
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c;
    alert("You are approximately " + d.toString().substr(0, d.toString().indexOf(".") + 3) + " kilometers from the original family home in Quảng Lập, Đơn Phương, Lâm Đồng province Viet Nam");
}

function updateModal(d) {
    document.getElementById("modalContent").innerHTML = d.toString().substr(0, d.toString().indexOf(".") + 4) + " kilometers";
}
