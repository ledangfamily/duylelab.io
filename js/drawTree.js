/**
 * Source: https://sourceforge.net/projects/gedbrowser/
 * @version: Oct 5, 2017
 */
var historyList = new Array();
var lastHistory = -1;

function start() {
    var elem;
    elem=document.getElementById('infoJavascript');
    elem.style['display']='none';
    if (params['title']) {
        elem=document.getElementById('title');
        elem.innerHTML="";
    }
    if (params['hints']!='') {
        elem=document.getElementById('hints');
        elem.innerHTML=params['hints'].replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/\n/g,'<break />');
    }
    else start1();
}

function start1() {
    var elem, ind;
    elem=document.getElementById('infoPart');
    elem.style['display']='none';
    elem=document.getElementById('browserTab');
    elem.style['visibility']='visible';
    if (params['startWith']) showIndi(params['startWith']);
    else alert('No Individual-key found to start with.');
}    

function getList(obj, key, suffix) { // returns an array with the values of all keys key + '.' + i + suffix
    var ret = new Array();
    var i = 0;
    while (true) {
        if (typeof(obj[key + '.' + i + suffix])=='undefined') return ret;
        ret[i]=obj[key + '.' + i + suffix];
        i++;
    }
    return ret;
}
    
function showIndi(ref) { // show the indiviudal ref
    var elem, elem1, indi, indi2, famList, famKey, fam, famInfo, childList, childKey, childInfo, spouse, spouseId, p;
    indi=ged[ref + '.0.'];
    
    if (!indi) {
        alert('Individual with key ' + ref  + ' not found.');
        return;
    }
    addHistory(ref);
    var male=true;
    if (indi['SEX.0']) male = (indi['SEX.0']=='M') || (indi['SEX.0']=='m');
    elem = document.getElementById('title');
    // elem.innerHTML = 'Family Tree ' + indi['NAME.0'].replace(/\//g, '');
    elem = document.getElementById( male ? 'g12' : 'g13');
    elem.innerHTML = getIndiInfo(ref,1,0,1); // Person
    showIndiPic(ref,male ? 'p12' : 'p13'); // picture
    elem = document.getElementById( male ? 'g11' : 'g14');
    elem.innerHTML = getSiblingsInfo(ref);
    elem = document.getElementById( male ? 'g13' : 'g12');
    elem.innerHTML='';
    elem1 = document.getElementById( male ? 'g14' : 'g11');
elem1.innerHTML='';
    famList=getList(indi,'FAMS','');
    famInfo='';
    childInfo='';
    spouseId=0;
    for (famKey in famList) {
        fam=ged[famList[famKey] + '.0.'];
        tmp = male ? 'WIFE.0' : 'HUSB.0';
        if (fam[tmp]) {
            indi2 = ged[fam[tmp] + '.0.'];
            elem.innerHTML+=getIndiInfo(fam[tmp],1,1+(famKey-0), famList.length); // Gatte/Partner
            elem1.innerHTML+=getSiblingsInfo(fam[tmp]);
        }
        if (famKey==0) {
            famInfo=getFamInfo(fam);
            spouse = indi2;
            spouseId=fam[tmp];
        }
        childList=getList(fam,'CHIL','');
        for (childKey in childList) {
            childInfo+=getIndiInfo(childList[childKey],0,1+(famKey-0), famList.length); // Kinder
        }
    }
    showIndiPic(spouseId,male ? 'p13' : 'p12');
    elem = document.getElementById('ehe');
    elem.innerHTML=famInfo;
    elem = document.getElementById('childrenCol'); // IE does not support assigning innerHTML to a tbody; this is a workaround
    p=elem.innerHTML.indexOf('<TBODY id=');
    if (p<0) p=elem.innerHTML.indexOf('<tbody id=');
    p=elem.innerHTML.indexOf('>',p);
    childInfo=elem.innerHTML.substr(0,p+1) + childInfo + '</tbody></table>';
    elem.innerHTML=childInfo;
    //elem.innerHTML=childInfo;
    assignValue(getObjKey(indi,'FAMC.0/HUSB.0'),2, male ? 1 : 3);
    assignValue(getObjKey(indi,'FAMC.0/WIFE.0'),2, male ? 2 : 4);
    assignValue(getObjKey(spouse,'FAMC.0/HUSB.0'),2, male ? 3 : 1);
    assignValue(getObjKey(spouse,'FAMC.0/WIFE.0'),2, male ? 4 : 2);
    assignValue(getObjKey(indi,'FAMC.0/HUSB.0'),3, male ? 1 : 3);
    assignValue(getObjKey(indi,'FAMC.0/WIFE.0'),3, male ? 2 : 4);
    assignValue(getObjKey(spouse,'FAMC.0/HUSB.0'),3, male ? 3 : 1);
    assignValue(getObjKey(spouse,'FAMC.0/WIFE.0'),3, male ? 4 : 2);
    elem=document.getElementById('lastVisits');
    if (elem)
        elem.innerHTML=getHistory();
    elem=document.getElementById('info'); // IE does not support assigning innerHTML to a tbody; this is a workaround
    if (elem)
        p=elem.innerHTML.indexOf('<TBODY id=');
    if (p<0) p=elem.innerHTML.indexOf('<tbody id=');
    if (elem) {
        p=elem.innerHTML.indexOf('>',p);
        elem.innerHTML=elem.innerHTML.substr(0,p+1) + getDetailInfo(indi,0,ref) + '</tbody></table>';
    }
}

function getObjKey(base, path) {
    var ind, ret, tmp;
    var pathList=path.split('/');
    if (!base) return;
    obj=base;
    for (ind in pathList) {
        tmp=pathList[ind];
        if (!obj[tmp]) return;
        ret = obj[tmp];
        obj=ged[ret + '.0.'];
        if (!obj) return;
    }
    return ret;
}

function assignValue(key, level, nr) {
    var elem=document.getElementById('g' + level + nr);
    if (!key) elem.innerHTML='';
    else if (level<3) elem.innerHTML=getIndiInfo(key,level,0,0);
    else {
        var obj=ged[key + '.0.'];
        elem.innerHTML = getIndiInfo(getObjKey(obj,'FAMC.0/HUSB.0'),level,0,0) + getIndiInfo(getObjKey(obj,'FAMC.0/WIFE.0'),level,0,0);
    }
}

function addHistory(ref) {
    // global historyList, lastHistory
    if (lastHistory==19) lastHistory=0;
    else if (historyList.length<20) lastHistory=historyList.length;
    else lastHistory++;
    historyList[lastHistory]=getIndiInfo(ref,6,0,0);
}
        
function getHistory() {
    var ret,ind1, ind2;
    ret='';
    ind1=lastHistory;
    for (ind2=0;ind2<historyList.length;ind2++) {
        ret+=historyList[ind1];
        if (ind1==0) ind1=historyList.length-1;
        else ind1--;
    }
    return ret;
}

function showIndiPic(ref,elemName) { // returns
    var elem, p, indi, objList, fName, suffix;
    elem=document.getElementById(elemName);
    elem.style['display']='none';
    if (!ref) return;
    indi=ged[ref + '.0.'];
    if (!indi) return;
    objList=getList(indi,'OBJE','.');
    for (ind in objList) {
        fName=objList[ind]['FILE.0'];
        if (fName) {
            p=fName.lastIndexOf('\\');
            if (p>0) fName=fName.substr(p+1);
            suffix=fName.substr(fName.length-4).toLowerCase();
            if (suffix=='.jpg' || suffix=='.bmp' || suffix=='.tif' || suffix=='.gif') {
                elem.src='img\\' + fName;
                elem.style['display']='block';
                break;
            }
        }
    }
}
     
            

function getIndiInfo(ref,level,famNr,famCount) { // level=0 : Kinder, =1: Eltern, =2: Gro�eletern, =3: Urgro�eltern, =4: Tante/Onkel, =5: deren Gatten/Partner; =6 history
    var tmp, gebDat, gebOrt, todDat, todOrt, famList, famKey, fam;
    if (!ref) return '';
    var indi=ged[ref + '.0.'];
    var jsFunc= '"javascript:showIndi(' + "'" + ref + "')" + '"';
    var ret='<div class="div' + level + '">';
    var indiName=indi['NAME.0'];
    var p1 = indiName.indexOf('/');
    var p2 = indiName.indexOf('/',p1+1);
    if ((level!=5) && (p1>=0) && (p2>0)) indiName=indiName.substr(0,p1) + ( (level!=4) ? '<strong>' + indiName.substr(p1+1, p2-p1-1) + '</strong>' : '') + indiName.substr(p2+1);
    else if ((p1>=0) && (p2>0)) indiName=indiName.substr(0,p1) + indiName.substr(p1+1, p2-p1-1) + indiName.substr(p2+1);
    if (indiName.substr(indiName.length-1)==' ') indiName=indiName.substr(0,indiName.length-1);
    if (level==4) indiName='<strong>' + indiName +'</strong>';
    indiName='<a class="drop" href=' + jsFunc + ' title="Switch to the pedegree of this person">' + indiName + '</a>';
    indiName = addDropdown(indiName, ref);
    // indiName='<a href=' + jsFunc + ' title="Switch to the pedegree of this person"' + 'onclick="' +'updateModal('+"'"+ref+"'"+')'+'"' + 'data-toggle="modal" data-target="#exampleModal">' + indiName + '</a>';
    var geb = indi['BIRT.0.'];
    if (geb) {
        gebDat=geb['DATE.0'];
        gebOrt=geb['PLAC.0'];
    }
    var tod = indi['DEAT.0.'];
    if (tod) {
        todDat=tod['DATE.0'];
        todOrt=tod['PLAC.0'];
    }
    var male=true;
    if (indi['SEX.0']) male = (indi['SEX.0']=='M') || (indi['SEX.0']=='m');
    if (level==0) {
        ret='<tr><td>' + indiName + '</td><td>' + formatDate(gebDat,0) + '</td><td>' + ( gebOrt ? gebOrt : '');
        famList=getList(indi,'FAMS','');
        for (famKey in famList) {
            fam=ged[famList[famKey] + '.0.'];
            tmp = male ? 'WIFE.0' : 'HUSB.0';
            if (fam[tmp]) {
                ret+= getIndiInfo(fam[tmp],3,0,0);
            }
        }
        ret+='</td></tr>';
    }
    else if (level==1) {
        ret += (famCount>1) ? '#' + famNr + ' ' : '';
        ret += indiName + '<br />' + (gebDat ? formatDate(gebDat,1) + '<br />' : '') + ( gebOrt ? gebOrt + '<br />' : '');
        ret += (todDat ? (formatDate(todDat,2) + '<br />') : '');
        ret += ( todOrt ? (todOrt + '<br />') : '') + '</div>';
    }       
    else if (level>=2) {
        if (level==5) ret += ' <span class="oo">oo</span>';
        ret += indiName;
        if ((level<=3) || (level==6)) ret += ' ' + (gebDat ? formatDate(gebDat,1) : '');
        ret += '</div>';
    }       
    return ret;   
}

function getFamInfo(fam) {
    var marDat, marOrt;
    var ret='<div>';
    var mar = fam['MARR.0.'];
    if (mar) {
        marDat=mar['DATE.0'];
        marOrt=mar['PLAC.0'];
    }
    ret += (marDat ? formatDate(marDat,3) + ' ' : '') + ( marOrt ? marOrt : '') + '</div>';
    return ret;   
}

function getSiblingsInfo(ref) {
    var indi, famKey, fam, childList, tmp, indiKey, famList1, famList2, ind1, ind2, ind3, ind1, male, ret, tmp;
    ret='';
    if (!ref) return '';
    indi = ged[ref + '.0.'];
    if (!indi) return '';
    famKey=indi['FAMC' + '.0']; //family of parents
    if (!famKey) return '';
    fam = ged[famKey + '.0.'];
    if (!fam) return '';
    famList1=new Array();
    famList1=getFAMS(fam,'HUSB.0',famList1);
    famList1=getFAMS(fam,'WIFE.0',famList1);
    for (ind1 in famList1) {
        ret+='<div class="famSibling">';
        if (famList1.length>1) ret+='�';
        fam=ged[famList1[ind1] + '.0.'];
        childList = getList(fam,'CHIL','');
        for (ind2 in childList) {           
            indiKey=childList[ind2];
            if (ind2>0) ret+='<div class="separator">,</div>';
            ret+='<div class="sibling">';
            if (indiKey==ref) ret+='*';
            else {
                ret+=getIndiInfo(indiKey,4,0,0);
                indi=ged[indiKey + '.0.'];
                male=true;
                if (indi['SEX.0']) male = (indi['SEX.0']=='M') || (indi['SEX.0']=='m');
                famList = getList(indi,'FAMS','');
                for (ind1 in famList) {
                    famKey=famList[ind1];
                    fam = ged[famKey + '.0.'];
                    indiKey=fam[male ? 'WIFE.0' : 'HUSB.0'];
                    if (indiKey) ret+=getIndiInfo(indiKey,5,0,0);
                }
        }
            ret+='</div>';
        }
        ret+='</div>';
    }
    return ret;   
}

function getFAMS(fam,spouse,famList) { // returns all fams in famList and all FAMSpouse of the person fa[spouse)
    var ret, indi, famList, ind1, ind2, found, famKey;
    ret=famList;
    if (!fam[spouse]) return ret;
    indi=ged[fam[spouse] + '.0.'];
    if (!indi) return ret;
    famList=getList(indi,'FAMS','');
    for (ind1 in famList) {
        famKey=famList[ind1];
        found=false;
        for (ind2 in ret) {
            found = (ret[ind2]==famKey);
            if (found) break;
        } 
        if (!found) ret[ret.length]=famKey;
    }
    return ret;
}

function getDetailInfo(obj, level, ref) {
    var ret, obj, objKey, cont, obj1;
    ret='';
    for (objKey in obj) {
       cont=obj[objKey];
       if ((objKey.substr(0,5)!='CHIL.') && (objKey.substr(0,5)!='FAMC.') && (cont!=ref)) {
            ret+='<tr><td style="padding-left:' + (level*2) + 'em">' + objKey.substr(0,4) +'</td>';
            if (typeof(cont)=='object') ret+='</tr>' + getDetailInfo(cont,level+1,ref);
            else if ((cont.substr(0,1)=='@') && (cont.substr(cont.length-1)=='@')) {
                obj1=ged[cont+'.0'];
                if (obj1 && (cont!=ref) && (obj1=='INDI')) {
                    ret+='<td>' + getIndiInfo(cont,6,0,0) + '</td></tr>';
                }
                else {
                    obj1=ged[cont + '.0.'];
                    ret+='</tr>' + getDetailInfo(obj1,level+1,ref)
                }
            }
            else ret+='<td>' + cont +'</td></tr>';
        }
    }
    return ret;
}
            
           
    
    
    

function formatDate (d,sym) {
    if (!d) return '';
    var mList= '.JAN.FEB.MAR.APR.MAY.JUN.JUL.AUG.SEP.OCT.NOV.DEC.';
    var parts=d.match(/(\d*)\s*([A-Za-z]*)\s*(\d*)/);
    var ret='';
    if (sym>0) ret=' *+'.substr(sym,1) + ((sym==3) ? '<span class="oo">oo</span>' : '');
    if (parts[2]!='') {
        var p1=mList.indexOf('.' + parts[2] + '.');
        p1=p1/4+1;
        var p2=d.indexOf(parts[2]);
        if (p2==0) ret+= p1 + '.' + d.substr(4);
        else ret+= d.substr(0,p2-1) + '.' + p1 + '.' + d.substr(p2+4);
    }
    else ret+= d;
    return ret;
}
function search() {
    var ret, inp, dat, indi, erg, elem, p, gedKey;
    var mList= '.JAN.FEB.MAR.APR.MAY.JUN.JUL.AUG.SEP.OCT.NOV.DEC.';
    inp=prompt('Search','')
    if (!inp) return;
    dat=inp.match(/(\d+)\.\s+(\d+)\.\s+(\d+)/);
    if (dat) inp=dat[1] + ' ' + mList.substr(dat[2]*4-3,3) + dat[3];
    else dat=inp.match(/(\d+)/);
    inp=inp.toLowerCase();
    ret='';
    for (gedKey in ged) {
        if ((gedKey.substr(0,1)=='@') && (gedKey.substr(gedKey.length-3,1)=='@')) {
            if (ged[gedKey]=='INDI') {
                indi=ged[gedKey+'.'];
                erg=indi[dat ? 'BIRTH.0' : 'NAME.0'].replace(/\//,'');
                if (dat && erg) erg=erg['DATE.0'];
                if (erg) erg = erg.toLowerCase().indexOf(inp)+1;
                if (erg) ret+=getIndiInfo(gedKey.substr(0,gedKey.length-2),6,0,0);
            }
        }
    }
    elem=document.getElementById('info'); // IE does not support assigning innerHTML to a tbody; this is a workaround
    p=elem.innerHTML.indexOf('<TBODY id=');
    if (p<0) p=elem.innerHTML.indexOf('<tbody id=');
    p=elem.innerHTML.indexOf('>',p);
    elem.innerHTML=elem.innerHTML.substr(0,p+1) + '<tr><td colspan="2">' + ret + '</td></tr></tbody></table>';
}      

/**
 * Returns person birth year
 * @param {Object} person tree object
 * @return {string} the year the person was born
 */
function getPersonBirthYear (person) {
    return (person['BIRT.0.'] == null) ? -1: person['BIRT.0.']['DATE.0'];
}

/**
 * Update modal's content
 */
function updateModal(ref) {
    const indi=ged[ref + '.0.'];
    document.getElementById("modalContent").innerHTML = getPersonBirthYear(indi);
    document.getElementById("exampleModalLabel").innerHTML = getPersonBirthYear(indi);
    console.log(getPersonBirthYear(indi));
}

/**
 * Adds dropdown content to string
 * @param {string} nameLink link contains <a></a> structure 
 * @param {Object} ref reference to person object 
 */
function addDropdown(nameLink, ref) {
    // indiName='<a href=' + jsFunc + ' title="Switch to the pedegree of this person"' + 'onclick="' +'updateModal('+"'"+ref+"'"+')'+'"' + 'data-toggle="modal" data-target="#exampleModal">' + indiName + '</a>';
    nameLink += "<div class='dropdown'>" +
        "<div class='dropdown-content'>"+
        "</div></div>";
    // updateModal(ref);
    return nameLink;
}
